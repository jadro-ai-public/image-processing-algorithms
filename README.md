Implementations of almost all Image Processing algorithms from the following two books:

*  [Principles of Digital Image Processing: Fundamental Techniques (Wilhelm Burger)](https://www.amazon.com/gp/product/1848001908/ref=dbs_a_def_rwt_bibl_vppi_i1)
*  [Principles of Digital Image Processing: Core Algorithms (Wilhelm Burger, Mark J. Burge)](https://www.amazon.com/gp/product/1848001940/ref=dbs_a_def_rwt_bibl_vppi_i2)

![](https://gitlab.com/jadro-ai-public/image-processing-algorithms/-/raw/master/images/book_1_cover.jpg)
![](https://gitlab.com/jadro-ai-public/image-processing-algorithms/-/raw/master/images/book_2_cover.jpg)
